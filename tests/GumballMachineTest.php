<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

/**
* GumballMachineTest
*/
final class GumballMachineTest extends TestCase
{
	public $gumballMachineInstance;

	function setUp()
	{
		$this->gumballMachineInstance = new GumballMachine();
	}

	public function testIfWheelWorks(){
		$this->gumballMachineInstance->setGumballs(100);
		$this->gumballMachineInstance->turnWheel();
		$this->assertEquals(99, $this->gumballMachineInstance->getGumballs());
	}
}